# Welcome to Linux Vibe!

<!-- ![myimage](https://gitlab.com/agrinshpon/cloudshell_test/raw/main/images/logo.png) -->

```bash
docker compose up -d; docker exec -it ubuntu /bin/bash; clear
```

```bash
docker rm -vf $(docker ps -aq); docker rmi -f $(docker images -aq)
```

```bash
docker system prune --all
```

## Chapter 1: Intro to the Linux Shell

This is what you will learn in this chapter:

### Shells

We will cover the basics of shells and some of the different types.

### Commands

We will talk about Linux commands and their syntax.

### Variables

We will talk about variables and environment variables in Linux.

### Getting Help

We will look at the different options for getting help with Linux commands.

### The Filesystem

We will examine the Linux filesystem and see how it differs from Windows.

## Shells

### What is a shell?

A **shell** in a Linux system is a program that lets you **interact with the operating system**. It takes input from your keyboard and uses that input to tell the computer what to do.

![shell](https://gitlab.com/agrinshpon/cloudshell_test/raw/main/images/shell.png)

### What shell am I using?

To see the shell that you are currently using type in the following command and hit enter:

```bash
echo $SHELL
```

I'm sure you have a lot of questions right now:
1. What is a command?
2. What does `echo` mean?
3. What does the $ in front of the word *SHELL* mean?
4. What does the output /bin/bash mean?

We will be covering all of these questions in the upcoming lessons. For now, just know that the output /bin/bash let us know that we are using the **bash** shell.

### More on Shells

The **bash** shell is the **default shell** on most Linux systems and the one we will be working with in this course. It stands for Bourne Again Shell and it is an improved version of the Bourne Shell, which was the first shell on Unix systems. Different shells have different features and if you are interested in diving deeper into other shells, here are a few popular alternatives:

1. Fish 
2. Zsh
3. Korn

Let's learn about commands in the next lesson.

## Commands

### What is a command?

A Linux **command** is an **instruction** or **set of instructions** that you pass into the shell to interact with your system.

### Command Syntax

See the image below which describes the syntax of a linux command.

![syntax](https://gitlab.com/agrinshpon/cloudshell_test/raw/main/images/syntax.png)

As you can see in the picture, you can pass **options** and **arguments** into commands.

**Options** are built into the command and **change the default behavior** of the command. For example, certain commands have options that let you output additional logging information that isn't output by default.

**Arguments** are the items that you **input into your command** that become the target for the command. All options are technically arguments, but not all arguments are options. 

### Echo Example

Let's take a look at the command we ran in the last lesson:

```bash
echo $SHELL
```

In this example, `echo` is the command and *$SHELL* is the argument.

In a later lesson we will talk about how to find out what commands do, but for now just know that the `echo` command lets you display lines of text that you pass in as arguments.

Let's confirm this by running some `echo` commands:

```bash
echo "Hello World"
```

```bash
echo SHELL
```

### Echo - Default Behavior

Let's verify that passing in the argument *$SHELL* changed the default behavior of the echo command. Try running the `echo` command without any options:

```bash
echo
```

We can see that the default behavior of the echo command just displays a new line.

### Echo - Options

Now let's try and pass an option into the command and see how it alters the command's behavior. We will start with the *-n* option. This option removes the new line that echo adds into it's output by default. Let's run the command:

```bash
echo -n $SHELL
```

By now, we know enough to answer our first 2 questions:
1. What is a command?
2. What does `echo` mean?

We will explore ways to find out what different options are available to a command in an upcoming lesson. For now, consider this: I just said that the `echo` command displays text that the user inputs, but we input *$SHELL* and the output we got was /bin/bash. How did that happen?

This is because *$SHELL* is a **variable**. We will take a look at variables in the next section and this will help us answer our 3rd question:
1. What does the $ in front of the word *SHELL* mean?

## Variables

A **variable** is a piece of data that is stored by the shell. Programs often use the data stored in variables to get information, and the behavior of the program may change based on the value of the variable. 

### Environment Variables

The lifespan of a variable is usually limited to it's current shell and is not shared with the shell's child processes, however, there are a subset of variables called **environment variables** which are variables that are loaded into any new shell by default and are shared with the shell's child processes.

To list the current environment variables in your shell, you can run the following command:

```bash
printenv
```

You can also list an individual environment variable like this:

```bash
printenv SHELL
```

Or like this using the `echo` command, like we've done before:

```bash
echo $SHELL
```

The `echo` command can list variables that we set, not just environment variables. Let's test this out.

### Setting Variables

To set a variable we can declare it like this:

```bash
VIBES='good'
```

Now let's see if we can see it using the `printenv` command:

```bash
printenv VIBES
```

Now let's see if we can see it using the `echo` command:

```bash
echo $VIBES
```

### Setting Environment Variables

To set environment variables, we can use the `export` command:

```bash
export VIBES='good'
```

Now let's verify we can see it using `printenv`:

```bash
printenv VIBES
```

### Converting Variables to Environment Variables

We can also use the `export` variable to convert variables to environment variables.

Let's create a new variable:

```bash
PET='cat'
```

Let's export it:

```bash
export PET
```
And let's verify that it is indeed an environment variable:

```bash
printenv PET
```

We will take a further look into variables and environment variables when we get into the scripting and administration sections of the course, but for now we have everything we need to answer the top 3 questions:
1. What is a command?
2. What does `echo` mean?
3. What does the $ in front of the word *SHELL* mean?

Let's continue and answer the final question:
1. What does the output /bin/bash mean?

## The Linux Filesystem

To answer the final question (what does /bin/bash mean?) from our list of questions in the 1st section, we need to understand the Linux filesystem. 

**Insert Picture**

You can think of the Linux filesystem as a tree. As you can see in the photo, everything branches off from the **root directory**, which is signified as **/**.

The table below shows some of most important directories in a Linux system and what is inside of them. There is no need to memorize the table as you will gain familiarity with each directory as we go through the course.

| Directory Name | What's in here?                                                                                  |
|----------------|--------------------------------------------------------------------------------------------------|
| /bin           | Binaries (or applications). The echo command we ran before is an example of a binary.            |
| /boot          | Important files needed for booting the system                                                    |
| /dev           | Device files for things like USBs and external hard drives                                       |
| /etc           | System-wide configuration files                                                                  |
| /home          | Users' personal directories.                                                                     |
| /lib           | Library files that applications on the system share                                              |
| /media         | Mount point for external storage like USBs and external hard drives                              |
| /mnt           | Not used often anymore, but can be a mount point for filesystems                                 |
| /opt           | Software packages from third party vendors                                                       |
| /proc          | Virtual filesystem that has information about system resources                                   |
| /root          | Home directory for the root user (not to be confused with the root directory).                   |
| /sbin          | System binaries                                                                                  |
| /tmp           | Temporary data. Files here  are deleted when rebooting the system.                               |
| /usr           | Libraries and binaries for installed software. Items in this directory are shared amongst users. |
| /var           | Variable data like logs, database files, and user mailboxes                                      |

### Answering the last question

As you can see from the table, the `/bin/` part of `/bin/bash` refers to a directory. That must mean there is a binary called `bash` inside of the `/bin/` directory. 

Run this command and find out if that is true:

```bash
ls /bin/ | grep bash
```

We can see that there is indeed a binary called `bash` inside of the `/bin/` directory!

### Putting It All Together

So now we can see that the `echo $SHELL` command prints out the data that is stored in the *$SHELL* environment variable, and that data refers to a binary called `bash` which is stored in the `/bin/` directory.

### Next Questions

You ran two commands above that we have not seen before - the `ls` and the `grep` commands. We will see how we can leverage Linux' built-in help documentation to figure out what they do. 

We also saw a **|** character in between the `ls` and `grep` commands. We will be covering that in a future section. 

## Getting Help

There are so many Linux commands. Do we need to memorize all of them? And how do we know which options are available for a given command? Luckily, we don't need to memorize anything. You will gain familiarity with many different commands from just going through this course and practicing, but for any other ones you may have forgotten, there are a ton of resources out there to get help with them.

### Help Option

Luckily, most bash commands have a built-in help option that gives you a quick synopsis about what it does and what options you have available to you.

Let's explore the help pages for the `printenv` and `ls` commands:

```bash
printenv --help
```

```bash
ls --help
```

### Man Pages

Linux also has built-in manual pages that we can reference by calling the `man` command.

Let's try to look at the `echo` and `ls` man pages. Going through the man pages with your arrow keys is cumbersome so here are some shortcuts to help you:

| Key         | Purpose                                       |
|-------------|-----------------------------------------------|
| f/space bar | Go one full screen ahead                      |
| b           | Go one full screen back                       |
| j           | Go one line ahead                             |
| #j          | Go # lines ahead. Substitute # with a number. |
| k           | Go back one line                              |
| q           | Quit                                          |

```bash
man echo
```

```bash
man ls
```

The man pages are divided into the following sections:

| Section Number | Purpose                                  |
|----------------|------------------------------------------|
| 1              | General Commands                         |
| 2              | System Calls                             |
| 3              | Library Functions                        |
| 4              | Special Files                            |
| 5              | File Formats and Configuration Files     |
| 6              | Games                                    |
| 7              | Overview, Conventions, and Miscellaneous |
| 8              | System Management Commands               |

Why is this useful? Let's say we wanted to add a new user to the system, but we forgot what the command was. We can use a keyword search with the `man` command by adding the *-k* option.

```bash
man -k user
```

This gives us a big, long list of commands that involve the word 'user'. One particular entry may catch your eye in the line **useradd (8)**. From the description + the number 8, which refers to the section about system management, we can see that this is the command we needed!

### Whatis

`Whatis` is a helpful command that gives you a short description about what a certain command does. This can be helpful if you don't want to go through an entire man page.

Let's see some examples.

```bash
whatis echo
```

```bash
whatis ls
```

## Moving Around the Filesystem

In the last lesson, we learned about the filesystem. Now we're going to learn how to orient ourselves in a Linux system and traverse it.

### Where are we now?

To find out where you currently are, you can run the `pwd` command. It stands for **print working directory**.

```bash
pwd
```

### Changing Directories

To change directories, we can use the `cd` command, which stands for **change directory**. To start changing which directory we are in, we need to first learn about paths.

An **absolute path** is a path that starts from the root directory, or in other words, it lets you know where the target is with respect to the root directory. This means it starts with a `/`.

Example: `/bin/bash` tells us that `bash` is in the `bin/` directory in the `/` directory

A **relative path** is a path that starts from where you currently are. 

Example: Let's say we are in the `/` directory. `bin/` would be a relative path and `/bin/` would be an absolute path. In this case we can omit the leading `/` in the relative path because we are already in that directory.

There are some other useful shortcuts for moving around directories:

| Shortcut | Purpose                                                                      |
|----------|------------------------------------------------------------------------------|
| .        | Current directory                                                            |
| ..       | Parent directory (the one above your current directory in the hierarchy)     |
| -        | Your user's home directory (we will cover this later)                        |
| ~        | Previous directory (the directory you were at right before your current one) |

Let's start moving around!

First, we'll go to our user's home directory. Don't worry about what this means just yet as we will cover this when we talk about users.

```bash
cd ~
```

Let's see where we are:

```bash
pwd
```

Next, let's go back a directory:

```bash
cd ..
```

Now let's see where we are:

```bash
pwd
```

Let's go back to our previous directory:

```bash
cd -
```

And let's see where we are:

```bash
pwd
```

Excellent, we are right back where we started!

We could have also gotten back here using absolute and relative paths. Let's try that.

Go back a directory:

```bash
cd ..
```

```bash
pwd
```

Now let's use the relative path of `student` to go back to `/home/student`.

```bash
cd student
```

```bash
pwd
```

Now let's use an absolute path to go back to the `/home` directory:

```bash
cd /home
```

```bash
pwd
```

### What's in a Directory?

Now that we can move around the Linux filesystem, let's try and get some more information as to what's actually in these directories. 

We can list a directory's contents using the `ls` command.

Let's see what is in our home directory:

```bash
cd ~
```

```bash
ls
```

This shows us different files and directories in the current directory we are in. We can also set a target for the `ls` command to see the contents of that directory.

```bash
ls /home
```

This doesn't really give us too much information besides a list of names. Let's use the *-l* option to get some more info.

```bash
ls -l ~
```



