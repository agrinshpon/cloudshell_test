FROM ubuntu:22.04

# Update
RUN apt-get update && apt-get upgrade && apt-get -y install sudo

# Add user
RUN useradd -U -u 1500 -m student

# Sudo privileges
RUN usermod -aG sudo student

# Change password
RUN echo 'student:infrastructured' | chpasswd

USER student